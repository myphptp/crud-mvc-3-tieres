<?php
namespace metier\modelProduit;
include_once __DIR__."/../dao/dao.php";
include_once __DIR__."/model/produit.php";
use dao\dao\DAO;

use metier\produit\Produit;


class ModelProduit{
    private $server;
    public function __construct($tab)
    {
        $this->server = new DAO($tab);
    }

    // Get produit
    public function getAll()
    {
        return $this->server->getAll();
    }
    // Get produit
    public function find($champ, $valeur)
    {
        return $this->server->find($champ, $valeur);
    }
    // Add produit
    public function add($obj)
    {
        return $this->server->addOne($obj);
    }
    
    // delete produit
    public function delete($champ, $valeur)
    {
        return $this->server->deleteOneOrMany($champ, $valeur);
    }

    public function open()
    {
        return $this->server->openConnexion();
    }
    
    public function close()
    {
        return $this->server->openConnexion();
    }
}



// $mdCl = new ModelClient("client");
// $mdCl->add($cl);


?>