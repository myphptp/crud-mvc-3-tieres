<?php

namespace metier\model\categorie;
class Categorie{
    private $idCat;
    private $designCat;
    public function __construct(string $designation, $id = '')
    {
        $this->idCat = $id;
        $this->designCat = $designation;
    }
    
    // Getter // Setter
    public function getIdCat(){
        return $this->idCat;
    }
    
    public function getDesignCat(){
        return $this->designCat;
    }
    
    public function setDesignCat(string $value){
        $this->designCat = $value;
    }
    


    
}
?>