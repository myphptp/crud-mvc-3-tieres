<?php

namespace metier\model\produit;
class Produit{
    private $codeP;
    private $design;
    private $qteS;
    private $pu;
    private $categorie;
    private $img;
    public function __construct(string $code, string $des, int $qte, float $pu, int $cat, string $img)
    {
        $this->codeP = $code;
        $this->design = $des;
        $this->qteS = $qte;
        $this->pu = $pu;
        $this->categorie = $cat;
        $this->img = $img;
    }
    
    // Getter // Setter
    public function getCodeP(){
        return $this->codeP;
    }
    
    // public function setCodeP(string $value){
    //     $this->codeP = $value;
    // }

    public function getDesign(){
        return $this->design;
    }
    
    public function setDesign(string $value){
        $this->design = $value;
    }
    
    public function getQteS(){
        return $this->qteS;
    }
    
    public function setQteS(int $value){
        $this->qteS = $value;
    }

    public function getPu(){
        return $this->pu;
    }
    
    public function setPu(float $value){
        $this->pu = $value;
    }

    public function getCategorie(){
        return $this->categorie;
    }
    
    public function setCategorie(int $value){
        $this->categorie = $value;
    }

    public function getImg(){
        return $this->img;
    }
    
    public function setImg(string $value){
        $this->img = $value;
    }
    


    
}
?>