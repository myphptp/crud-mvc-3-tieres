<?php 
namespace dao\dao;
include_once "baseDonnees.php";
// include_once "../metier/model/produit.php";
// include_once "../metier/model/categorie.php";
use dao\baseDonnees\BD;
use \Exception;
// use metier\model\categorie\Categorie;
// use metier\model\produit\Produit;
use \PDO;



class DAO {
    private static $con;
    private $table;
    public function __construct($tab){
        $this->table = $tab;
        $this->openConnexion();
    }

    // etablire la connection avec la base de donnees 
    public function openConnexion(){
        self::$con = new PDO(BD::CHAINE_CONNEXION, BD::USER,BD::PASSWORD);
        self::$con->exec("SET CHARACTER SET utf8");
    }

    
    // fermer la connection avec la base de donnees 
    public function closeConnexion(){
        self::$con = null;
    }

    public function getAll(){
        $result=self::$con->query("select * from ".$this->table);
        return $result->fetchAll(PDO::FETCH_OBJ);
    }
    public function find($champ, $valeur){
        $stmt=self::$con->prepare("select * from $this->table where $champ = '$valeur'");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function deleteOneOrMany($champ, $valeur){
        $stmt=self::$con->prepare("delete from $this->table where $champ = '$valeur'");
        $stmt->execute();
        return $stmt;
    }

    public function addOne($obj){
        $class = get_class($obj);
        $s = explode("\\",$class);
        $x= end($s);
        // Class name
        $class = strtolower($x);

        // Proprieties name
        $objArr = (array) $obj; // Obj to Array (proprieties ==> keys)
        $prop = array_keys($objArr); // ==> liste des Keys (proprieties) 
        
        
        // preparer la requete

        $req = "INSERT INTO `$class` VALUES(";
        foreach ($prop as $key) {
            if ($prop[count($prop)-1] == $key and gettype($objArr[$key]) != "string") {
                $req .= "$objArr[$key]";
            }elseif ($prop[count($prop)-1] == $key) {
                $req .= "'$objArr[$key]'";
            }elseif (gettype($objArr[$key]) != "string") {
                $req .= "$objArr[$key],";
            }else{
                $req .= "'$objArr[$key]',";
            }
        }
        $req .= ")";
        // echo $req;
        // Ajouter l'obj a la base de donnees
        $stmt = self::$con->prepare($req);
        $result = $stmt->execute();
        $msg = "";
        if($result == 1)
            $msg = "success";
        else
            $msg = "$result";
        return $msg;
    }
    // modifier ======================
    public function modify($obj){
        if ($this->table == "produit") {
            // Modifier un produit
            $result=self::$con->exec("
                UPDATE 
                `produit` SET 
                    `codeP`= '".$obj->getCodeP()."',
                    `design`= '".$obj->getDesign()."',
                    `qteS`= ".$obj->getQteS().",
                    `pu`= ".$obj->getPu().",
                    `categorie`= ".$obj->getCategorie().",
                    `img`= '".$obj->getImg()."'
                WHERE 
                    codeP = '".$obj->getCodeP()."'"
                );
        
        }elseif ($this->table == "categorie") {
            // Modifier une categorie
            $result=self::$con->exec("
                UPDATE 
                `categorie` SET 
                    `idCat`=".$obj->getIdCat().",
                    `designCat`='".$obj->getDesignCat()."',
                WHERE 
                    `idCat`= ".$obj->getIdCat()." "
            );

        }else {
            throw new Exception("Table_Name_Invalide!!",1);
        }
        // Message de retour
        $msg="";
        if ($result == 1) {
            $msg = "SUCCESS";
        }else {
            $msg = "ERROR";
        }
        return $msg;
    
    }

    // public function updateOneOrMany($champCritere,$valeur,$newObj){
    //     $class = get_class($newObj);
    //     $s = explode("\\",$class);
    //     $x= end($s);
    //     // Class name
    //     $class = strtolower($x);

    //     // Proprieties name
    //     $objArr = (array) $newObj; // Obj to Array (proprieties ==> keys)
    //     $prop = array_keys($objArr); // ==> liste des Keys (proprieties) 
        
        
    //     // preparer la requete

    //     $req = "UPDATE $class SET ";
    //     foreach ($prop as $key) {
    //         // recuperer le champ =========
    //         $champ = explode('\\',strtolower($key));
    //         $champ = end($champ);
    //         $champ = explode($class,$champ);
    //         $champ = end($champ);
    //         // ============================
    //         $req .= "$champ = ";
    //         if ($prop[count($prop)-1] == $key and gettype($objArr[$key]) != "string") {
    //             $req .= "$objArr[$key]";
    //         }elseif ($prop[count($prop)-1] == $key) {
    //             $req .= "'$objArr[$key]'";
    //         }elseif (gettype($objArr[$key]) != "string") {
    //             $req .= "$objArr[$key], ";
    //         }else{
    //             $req .= "'$objArr[$key]', ";
    //         }
    //     }
    //     $req .= " WHERE $champCritere = '$valeur';";
    //     echo $req;
    //     // Ajouter l'obj a la base de donnees
    //     $stmt = self::$con->prepare("$req");
    //     $result = $stmt->execute();
    //     $msg = "";
    //     if($result >= 1)
    //         $msg = "success";
    //     else
    //         $msg = "$result";
    //     return $msg;
    // }

    public function __destruct()
    {
        $this->closeConnexion();
    }

}      
// $d = new DAO('categorie');
// // $pr = new Produit('lap10', 'pc xxxxxxxxxxxxx v2', 10, 1500.60, 1, 'test.jpg');
// $c = new Categorie('pc gamer hhhhhhhhhhhhhhh',9);
// print_r($d->modify($c));
