-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2023 at 09:16 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vente`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `idCat` int(11) NOT NULL,
  `designCat` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`idCat`, `designCat`) VALUES
(1, 'Storage Devices '),
(2, 'Laptops'),
(3, 'Desktops'),
(4, 'Tablets'),
(5, 'Mini PC'),
(9, 'pc gamer xxxxxxxxx');

-- --------------------------------------------------------

--
-- Table structure for table `produit`
--

CREATE TABLE `produit` (
  `codeP` varchar(10) NOT NULL,
  `design` text NOT NULL,
  `qteS` int(11) NOT NULL,
  `pu` decimal(6,2) NOT NULL,
  `categorie` int(11) NOT NULL,
  `img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produit`
--

INSERT INTO `produit` (`codeP`, `design`, `qteS`, `pu`, `categorie`, `img`) VALUES
('aa', 'lorem impsum', 10, '10.98', 4, 'wallpaperflare.com_wallpaper(1).jpg'),
('lap1', 'Intel Ultra Slim Notebook Intel N3350 14.1 in', 12, '3000.00', 2, 'images/6254c7a0216450.71967347.jpg'),
('lap2', 'Pc portable 15.6 pouces, 8 go de RAM, 128 go/256 go/512 go/1 to de SSD', 93, '1800.00', 2, 'images/6254c904c89975.55907925.jpg'),
('lap3', '14.1 pouces Ordinateur Mini Portable Nouveau AKPAD', 5, '3500.00', 5, 'images/6254c967ea7857.67706840.jpg'),
('s1', 'Mini SD Memory Card 512GB Micro SD Flash Card', 20, '401.00', 1, 'images/storage1.jpg'),
('xx01', 'lorem impsum', 10, '10.98', 4, 'wallpaperflare.com_wallpaper(1).jpg'),
('xx02', 'lorem impsum', 10, '10.98', 5, 'wallpaperflare.com_wallpaper(1).jpg'),
('xxc', 'ssssssssssssss', 12, '11.97', 9, '1126221.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idCat`);

--
-- Indexes for table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`codeP`),
  ADD KEY `fk_categorie` (`categorie`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `fk_categorie` FOREIGN KEY (`categorie`) REFERENCES `categorie` (`idCat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
