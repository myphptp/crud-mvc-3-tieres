<!DOCTYPE html>
<html lang="en">
<?php 
    include_once __DIR__.'/../component/head.php';
?>

<body>
<?php 
    include_once __DIR__.'/../component/navbar.php';
?>
    
    <div class="container table-responsive px-0 py-5">
        <?php if ($result == 'success') : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            
                <strong>New product added</strong>
            </div>
            
        <?php endif; ?>

        <form method="post" >
            <div class="mb-3">
                <label for="code" class="form-label">Code Produit</label>
                <input type="text" name="codeP" class="form-control" id="code">
            </div>
            <div class="mb-3">
                <label for="des" class="form-label">Designation</label>
                <input type="text" name="design" class="form-control" id="des">
            </div>
            <div class="mb-3">
                <label for="qte" class="form-label">Qte en stock</label>
                <input type="number" min="1" step="1" name="qteS" class="form-control" id="qte">
            </div>
            <div class="mb-3">
                <label for="p" class="form-label">PU</label>
                <input type="number" min="1" step="0.01" name="pu" class="form-control" id="p">
            </div>
            <div class="mb-3">
                <div class="mb-3">
                    <label for="cat" class="form-label">Categorie</label>
                    <select class="form-select form-select-lg" name="categorie" id="cat">
                        <?php foreach ($lstCat as $c): ?>
                            <option value="<?= $c->idCat ?>"><?= $c->designCat ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Image</label>
                <input type="file" name="img" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" name="btnAdd" class="btn btn-primary">Add</button>
            <button type="reset" class="btn btn-secondary">Cancel</button>
        </form>
    </div>
    
 

<?php 
    include_once __DIR__.'/../component/footer.php';
?>
    
</body>
</html>