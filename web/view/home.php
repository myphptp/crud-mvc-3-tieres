<!DOCTYPE html>
<html lang="en">
<?php 
    include_once __DIR__.'/../component/head.php';
?>
<body>
  
<?php 
    include_once __DIR__.'/../component/navbar.php';
?>
    
<div class="container d-flex flex-column justify-content-center align-items-center" style="height: 60vh;">
   <h1>Welcome to my website</h1>
   <div>
    <a class="btn btn-dark" href="http://localhost/crud-mvc-3-tieres/product" role="button">Product</a>
    <a class="btn btn-secondary" href="http://localhost/crud-mvc-3-tieres/category" role="button">Category</a>
   </div>
</div> 
 

<?php 
    include_once __DIR__.'/../component/footer.php';
?>
    
</body>
</html>