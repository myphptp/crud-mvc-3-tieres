<!DOCTYPE html>
<html lang="en">
<?php 
    include_once __DIR__.'/../component/head.php';
?>

<body>
  <script>
    function confirmation(id) {
        if (confirm('vous etes sur de supprimer ce produit')) {
            window.location.href = "http://localhost/crud-mvc-3-tieres/product/delete/"+id;
        }
    }
</script>
<?php 
    include_once __DIR__.'/../component/navbar.php';
?>
    
    <div class="container table-responsive px-0 py-5">
        <h1 class=""><?= $titre ?></h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Code</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Qte en stock</th>
                    <th scope="col">PU</th>
                    <th scope="col">Categorie</th>
                    <th scope="col">Image</th>
                    <th scope="col">Operations</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lstProd as $pr): ?>
                <tr>
                    <td scope="row"><?= $pr->codeP ?></td>
                    <td><?= $pr->design ?></td>
                    <td><?= $pr->qteS ?></td>
                    <td><?= $pr->pu ?></td>
                    <td><?= $pr->categorie ?></td>
                    <td><?= $pr->img ?></td>
                    <td>
                        <a type="button" href="http://localhost/crud-mvc-3-tieres/product/<?= $pr->codeP ?>" class="btn btn-secondary">Details</a>
                        <button type="button" onclick="confirmation('<?= $pr->codeP ?>')" class="btn btn-danger">Delete</button>
                        <a type="button" href="http://localhost/crud-mvc-3-tieres/product/update/<?= $pr->codeP ?>" class="btn btn-success">update</a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr>
                <td colspan="7">
                    <div class="d-grid gap-2">
                        <a type="button" href="http://localhost/crud-mvc-3-tieres/product/add" class="btn btn-primary">New Produit</a>
                    </div>
                </td>
                    
                </tr>
        </table>
            </tbody>
                
    </div>
    
 

<?php 
    include_once __DIR__.'/../component/footer.php';
?>
    
</body>
</html>