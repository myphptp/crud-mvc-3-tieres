<!DOCTYPE html>
<html lang="en">
<?php 
    include_once __DIR__.'/../component/head.php';
?>
<body>
  
<?php 
    include_once __DIR__.'/../component/navbar.php';
?>
    
    <div class="container table-responsive px-0 py-5">
        <h1 class=""><?= $titre ?></h1>
        <table class="table table-striped">
            <tbody>
                <?php foreach ($prod as $pr): ?>
                <tr>
                    <th>Code</th><td><?= $pr->codeP ?></td></tr>
                    <tr><th>Designation</th><td><?= $pr->design ?></td></tr>
                    <tr><th>Qte en stock</th><td><?= $pr->qteS ?></td></tr>
                    <tr><th>PU</th><td><?= $pr->pu ?></td></tr>
                    <tr><th>Categorie</th><td><?= $pr->categorie ?></td></tr>
                    <tr><th>Image</th><td><?= $pr->img ?></td>
                </tr>
                <?php endforeach; ?>
                <tr>
                <td colspan="2">
                    <div class="d-grid gap-2">
                        <a type="button" href="http://localhost/crud-mvc-3-tieres/product" class="btn btn-secondary">Retour</a>
                    </div>
                </td>
                    
                </tr>
        </table>
            </tbody>
                
    </div>
    
 

<?php 
    include_once __DIR__.'/../component/footer.php';
?>
    
</body>
</html>