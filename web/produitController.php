<?php

namespace web\produitController;
include_once __DIR__."/../metier/modelProduit.php";

include_once __DIR__."/../metier/model/produit.php";

use metier\model\produit\Produit;
use metier\modelProduit\ModelProduit;

class ProduitController{
    private $model;
    public function __construct($tab)
    {
        $this->tableConnect($tab);

    }
    public function tableConnect($tab)
    {
        $this->model = new ModelProduit($tab);

    }

    public function index(){
        $page = ['','active',''];
        $titre = "Liste des Produits";
        $lstProd = $this->model->getAll();
        require_once __DIR__."/view/listProduit.php";
    }
    public function add($produit=null){
        $page = ['','active',''];
        $titre = "Ajouter Produit";
        $result = null;
        if ($produit) {
            $result = $this->model->add($produit);
        }
        $this->tableConnect('categorie');
        $this->model->open();
        $lstCat = $this->model->getAll();
        // var_dump($result);
        require_once __DIR__."/view/newPr.php";
    }

    public function detail(string $id){
        $page = ['','active',''];
        $titre = "Details Produit $id";
        $prod = $this->model->find('codeP', $id);
        require_once "view/detail.php";

        
    }

    public function delete(string $id)
    {
        $prod = $this->model->delete('codeP', $id);
        return $this->index();
    }
}



?>
